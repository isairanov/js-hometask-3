# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: 
> 1. Сохранить ссылку на this в переменную и передать ее в функцию: const that = this;
> 2. Использовать метод bind, чтобы явно передать контектс: 
> newContextMethod = someMethod.bind(targetContext);
> newContextMethod();
> 3. Использовать call/apply: func.call(context, arg1) — это обычный вызов функции, но с явно указанным this. apply принимает вторым аргументом массив, вместо перечисления через запятую.
> 4. С помощью стрелочной функции. Они не имеют своего this и захватывают значение из окружающего контекста

#### 2. Что такое стрелочная функция?
> Ответ: анонимные функции с особым синтаксисом, которые принимают фиксированное число аргументов и работают в контексте включающей их области видимости, то есть — в контексте функции или другого кода, в котором они объявлены.
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:
function Player(name, gender, side) {
    this.name = name;
    this.gender = gender;
    this.side = side;
    this.introduce = function () {
        console.log(`Name: ${this.name}. Gender: ${this.gender}. Side: ${this.side}`);
    };
}

```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
```
> Ответ:
```js
  const person = {
  name: 'Nikita',
  sayHello: function() {
    const that = this;
    setTimeout(function() {
      console.log(that.name + ' says hello to everyone!');
    }, 1000)
  }
};
  person.sayHello();
```
```js
const person = {
  name: 'Nikita',
  sayHello: function() {
    setTimeout(function() {
      console.log(this.name + ' says hello to everyone!');
    }.bind(this), 1000)
  }
};
person.sayHello();
```
```js
  const person = {
  name: 'Nikita',
  sayHello: function () {
    setTimeout(() => {
      console.log(this.name + ' says hello to everyone!');
    }, 1000);
  }
};
person.sayHello();
```


## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
